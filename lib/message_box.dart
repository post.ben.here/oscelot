import 'package:flutter/material.dart';
import 'package:oscelot/state.dart';
import 'package:get_it/get_it.dart';

class MessageBox extends StatefulWidget {
  MessageBox() : super();

  @override
  _MessageBoxState createState() => _MessageBoxState();
}

class _MessageBoxState extends State<MessageBox> {
  final _messageService = GetIt.instance.get<MessageService>();

  void initState(){
    super.initState();
    _messageService.addListener(update);
  }

  void update() {
    setState(() => {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            border: Border.all(
                width: 2.0, color: Theme.of(context).dividerColor)),
        child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Text(_messageService.messages, textAlign: TextAlign.left)));
  }

  @override
  void dispose() {
    _messageService.removeListener(update);
    super.dispose();
  }
}