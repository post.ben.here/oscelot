import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:oscelot/state.dart';
import 'package:oscelot/utils.dart';
import 'package:oscelot/message_box.dart';
import 'package:oscelot/connection_form.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ConnectionForm(),
          ElevatedButton(
            onPressed: () {
              sendTest(
                  GetIt.instance
                      .get<ConnectionService>()
                      .connection
                      .serverAddress,
                  GetIt.instance
                      .get<ConnectionService>()
                      .connection
                      .serverPort);
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.white10),
            ),
            child: const Text('Test'),
          ),
          Expanded(
            flex: 1,
            child: MessageBox(),
          )
        ]));
  }
}
