import 'dart:io';
import 'package:osc/osc.dart';
import 'dart:async';

void sendTest(InternetAddress destination, int port) {

  var msg = OSCMessage("/set", arguments: ["/dun/dap", 3, 3]);
  var bytes = msg.toBytes();
  var msg2 = OSCMessage.fromBytes(bytes);
  print(msg2.toString());

  void send(msg) {
    RawDatagramSocket.bind(InternetAddress.anyIPv4, 0).then((socket) {
      final bytes = msg.toBytes();
      socket.send(bytes, destination, port);
      socket.close();
    });
  }

  send(OSCMessage("/bind", arguments: ["/devor"]));
  Timer(Duration(seconds: 3), () {
    send(OSCMessage("/set", arguments: ["/devor", 8.0]));
  });
}

void conductTest() {

  final socket = OSCSocket(
      serverAddress: InternetAddress.loopbackIPv4,
      serverPort: 57131,
      destination: InternetAddress.loopbackIPv4,
      destinationPort: 57130
  );

  socket.listen((msg) {
    print("received: ${msg.toString()}");
  });


  Timer(Duration(seconds: 1), () {
    sendTest(InternetAddress.loopbackIPv4, 57131);
  });
}


InternetAddress toInternetAddress(String strAddr) {
  if (strAddr.toLowerCase() == "localhost") {
    return InternetAddress.loopbackIPv4;
  } else {
    return InternetAddress(strAddr);
  }
}

final _illegalAddressChars = RegExp('[#*,?]');

//' ', '#', '*', ',', '?', '[', ']', '{', '}'
bool isValidOscAddress(String address) =>
    address.isNotEmpty &&
        address[0] == '/' &&
        !_illegalAddressChars.hasMatch(address);