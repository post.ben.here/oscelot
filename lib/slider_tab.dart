import 'package:flutter/material.dart';
import 'package:oscelot/slider.dart';
import 'package:oscelot/state.dart';
import 'package:get_it/get_it.dart';

class SliderTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var controlService = GetIt.instance.get<ControlService>();
    var sliders = controlService.controls.asMap().entries
        .map((entry) => SynthSlider(index: entry.key)).toList();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: sliders),
    );
  }
}
