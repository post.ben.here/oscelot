import 'package:flutter/material.dart';
import 'package:oscelot/settings.dart';
import 'package:oscelot/slider_tab.dart';
import 'package:oscelot/xyslider_tab.dart';
import 'package:oscelot/state.dart';
import 'package:get_it/get_it.dart';

void main() {
  var messageService = MessageService();
  var controlService = ControlService(messageService: messageService);
  var connectionService = ConnectionService(
      connection: Connection(),
      messageService: messageService,
      controlService: controlService);

  final serviceLocator = GetIt.instance;
  serviceLocator.registerSingleton<MessageService>(messageService);
  serviceLocator.registerSingleton<ControlService>(controlService);
  serviceLocator.registerSingleton<ConnectionService>(connectionService);

  runApp(MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp() : super();

  @override
  _MainAppController createState() => _MainAppController();
}

////////////////////////////////////////////////////////
/// Controller holds state, and all business logic
////////////////////////////////////////////////////////
class _MainAppController extends State<MainApp>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  Widget build(BuildContext context) => _MainAppView(this, key: UniqueKey());

  @override
  void initState() {
    super.initState();
    var connectionService = GetIt.instance.get<ConnectionService>();
    connectionService.loadPrefs();
    _tabController = TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    var connectionService = GetIt.instance.get<ConnectionService>();
    connectionService.disconnectOsc();
    super.dispose();
  }
}

class _MainAppView extends StatelessWidget {
  final _MainAppController state;
  const _MainAppView(this.state, {required Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Oscelot",
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
            bottom: TabBar(
              controller: state._tabController,
              tabs: [
                Tab(icon: Icon(Icons.settings)),
                Tab(icon: Icon(Icons.bar_chart_rounded)),
                Tab(icon: Icon(Icons.bubble_chart_rounded)),
              ],
            ),
            title: Text('Oscelot'),
            leading: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(image: AssetImage('assets/icon/oscelot.png')))),
        body: TabBarView(
          controller: state._tabController,
          children: [
            Settings(),
            SliderTab(),
            XYSliderTab(),
          ],
        ),
      ),
    );
  }
}
