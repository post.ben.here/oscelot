import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oscelot/state.dart';
import 'package:get_it/get_it.dart';
import 'package:oscelot/utils.dart';

class ConnectionForm extends StatefulWidget {
  ConnectionForm() : super();

  @override
  _ConnectionFormState createState() => _ConnectionFormState();
}

class _ConnectionFormState extends State<ConnectionForm> {
  final _connectionService = GetIt.instance.get<ConnectionService>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController destController = TextEditingController();
  TextEditingController destPortController = TextEditingController();
  TextEditingController serverController = TextEditingController();
  TextEditingController serverPortController = TextEditingController();

  Connection connectionFromController() {
    return Connection(
        destination: toInternetAddress(destController.text),
        destinationPort: int.parse(destPortController.text),
        serverAddress: toInternetAddress(serverController.text),
        serverPort: int.parse(serverPortController.text));
  }

  void initState() {
    super.initState();
    GetIt.instance.get<ConnectionService>();
    _connectionService.addListener(update);
  }

  void update() {
    setState(() => {});
  }

  @override
  Widget build(BuildContext context) {
    var connection = _connectionService.connection;
    destController.text = connection.destination.address;
    destPortController.text = connection.destinationPort.toString();
    serverController.text = connection.serverAddress.address;
    serverPortController.text = connection.serverPort.toString();

    return Form(
      key: _formKey,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
          Widget>[
        TextFormField(
          controller: destController,
          decoration: const InputDecoration(
            labelText: 'Destination Address',
          ),
          validator: (String? value) {
            if (value == null || value.isEmpty) {
              return 'Please enter the destination address';
            }
            return null;
          },
        ),
        TextFormField(
          controller: destPortController,
          decoration: const InputDecoration(
            labelText: 'Destination Port',
          ),
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          validator: (String? value) {
            if (value == null || value.isEmpty) {
              return 'Please enter the destination port';
            }
            return null;
          },
        ),
        Row(children: [
          Expanded(
              child: TextFormField(
            controller: serverController,
            decoration: const InputDecoration(
              labelText: 'Server Address',
            ),
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
          )),
          IconButton(
              icon: const Icon(Icons.sync),
              color: Theme.of(context).indicatorColor,
              onPressed: _connectionService.syncServerAddressWifi)
        ]),
        TextFormField(
          controller: serverPortController,
          decoration: const InputDecoration(
            labelText: 'Server Port',
          ),
          validator: (String? value) {
            if (value == null || value.isEmpty) {
              return 'Please enter some text';
            }
            return null;
          },
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    var connInfo = connectionFromController();
                    _connectionService.configureAndConnectOsc(connInfo);
                  }
                },
                child: const Text('Connect'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.deepOrangeAccent),
                ),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    var connInfo = connectionFromController();
                    _connectionService.configureAndDisconnectOsc(connInfo);
                  }
                },
                child: const Text('Disconnect'),
              ),
            ),
            Container(
              height: 20.0,
              width: 20.0,
              decoration: BoxDecoration(
                  color:
                      _connectionService.connected ? Colors.green : Colors.red,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: _connectionService.connected
                          ? Colors.green
                          : Colors.red,
                      blurRadius: 5.0,
                      spreadRadius: 2.0,
                    ),
                  ]),
            ),
          ],
        ),
      ]),
    );
  }

  @override
  void dispose() {
    _connectionService.removeListener(update);
    super.dispose();
  }
}
