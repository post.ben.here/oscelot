import 'package:flutter/material.dart';
import 'package:oscelot/state.dart';
import 'package:get_it/get_it.dart';

class SynthSlider extends StatefulWidget {
  final int index;
  SynthSlider({required this.index}) : super();

  @override
  _SynthSliderState createState() => _SynthSliderState();
}

class _SynthSliderState extends State<SynthSlider> {
  final _controlService = GetIt.instance.get<ControlService>();
  final _connectionService = GetIt.instance.get<ConnectionService>();

  @override
  void initState() {
    super.initState();
    _controlService.addListener(controlUpdate);
    _connectionService.addListener(connectionUpdate);
  }

  void controlUpdate() {
    if (_controlService.didUpdateLast(widget.index)) {
      setState(() => {});
    }
  }

  void connectionUpdate() {
    setState(() => {});
  }

  bool isActive() {
    return _connectionService.connected &&
        _controlService.controls[widget.index].isActive();
  }

  void onChangeHandler(double val) {
    _connectionService.updateControlValue(widget.index, val);
  }

  @override
  Widget build(BuildContext context) {
    var control = _controlService.controls[widget.index];
    return Container(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 40),
        child: Column(children: [
          Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: Text(control.label)),
          Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: Text(control.clientValue.toStringAsFixed(2),
                style: TextStyle(
                    fontSize: 24,
                    color: Theme.of(context).toggleableActiveColor)),
          ),
          Expanded(
              child: RotatedBox(
                  quarterTurns: 1,
                  child: SliderTheme(
                      data: SliderThemeData(
                          thumbShape:
                              RoundSliderThumbShape(enabledThumbRadius: 20)),
                      child: Slider(
                          value: control.controlValue,
                          min: 0.0,
                          max: 1.0,
                          onChanged: isActive() ? onChangeHandler : null))))
        ]));
  }

  @override
  void dispose() {
    _controlService.removeListener(controlUpdate);
    _connectionService.removeListener(connectionUpdate);
    super.dispose();
  }
}
