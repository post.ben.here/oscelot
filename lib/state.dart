import 'dart:io';
import 'package:osc/osc.dart';
import 'package:flutter/material.dart';
import 'package:oscelot/utils.dart';
import 'package:wifi_info_flutter/wifi_info_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

const NOT_SET_LABEL = "Not Set";

class Connection {
  InternetAddress destination = InternetAddress("127.0.0.1");
  int destinationPort = 57130;
  InternetAddress serverAddress = InternetAddress("127.0.0.1");
  int serverPort = 57131;

  Connection({destination, destinationPort, serverAddress, serverPort}) {
    this.destination = destination ?? this.destination;
    this.destinationPort = destinationPort ?? this.destinationPort;
    this.serverAddress = serverAddress ?? this.serverAddress;
    this.serverPort = serverPort ?? this.serverPort;
  }
}

class MessageService extends ChangeNotifier {
  String _messages =
      "*** *** *** *** *** Debug Messages Will Scroll Here *** *** *** *** ***\n";

  void addMessage(String msg) {
    _messages += "$msg\n";
    notifyListeners();
  }

  String get messages => _messages;
}

class ConnectionService extends ChangeNotifier {
  OSCSocket? osc;
  bool _connected = false;
  Connection connection;
  MessageService messageService;
  ControlService controlService;

  ConnectionService(
      {required this.connection,
      required this.messageService,
      required this.controlService})
      : super();

  void loadPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    connection.destination = InternetAddress(
        prefs.getString('destination') ?? connection.destination.address);
    connection.destinationPort =
        (prefs.getInt('destinationPort') ?? connection.destinationPort);
    connection.serverAddress = InternetAddress(
        prefs.getString('serverAddress') ?? connection.serverAddress.address);
    connection.serverPort =
        (prefs.getInt('serverPort') ?? connection.serverPort);
  }

  updateConnection(Connection newConnection) {
    connection = newConnection;
    connectOsc();
  }

  void syncServerAddressWifi() async {
    var wifiIP = await WifiInfo().getWifiIP();
    connection.serverAddress =
        InternetAddress(wifiIP ?? connection.serverAddress.address);
    await configureOsc(connection);
  }

  Future<void> configureOsc(connInfo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    connection = connInfo;
    prefs.setString('destination', connInfo.destination.address);
    prefs.setInt('destinationPort', connInfo.destinationPort);
    prefs.setString('serverAddress', connInfo.serverAddress.address);
    prefs.setInt('serverPort', connInfo.serverPort);

    notifyListeners();
  }

  void configureAndConnectOsc(connInfo) async {
    await configureOsc(connInfo);
    this.connectOsc();
  }

  void configureAndDisconnectOsc(connInfo) async {
    await this.configureOsc(connInfo);
    this.disconnectOsc();
  }

  connectOsc() {
    osc?.close();
    osc = OSCSocket(
      serverAddress: connection.serverAddress,
      serverPort: connection.serverPort,
      destination: connection.destination,
      destinationPort: connection.destinationPort,
    );
    _connected = true;
    listenOsc();
    notifyListeners();
  }

  listenOsc() {
    osc?.listen((msg) {
      print("DEBUG OSC: Received ${msg.toString()}");
      if (msg.address == "/bind") {
        bindOsc(msg);
      } else if (msg.address == "/set") {
        setClientValue(msg);
      } else if (msg.address == "/unbind") {
        unbindOsc(msg);
      } else {
        messageService.addMessage("unknown message ${msg.toString()}");
      }
    });
  }

  // address: /bind
  // args: [label]
  void bindOsc(OSCMessage msg) {
    if (msg.arguments.length != 1) {
      messageService
          .addMessage("received /bind with malformed packet ${msg.toString()}");
      return;
    }
    var label = msg.arguments[0] as String;
    if (!isValidOscAddress(label)) {
      messageService
          .addMessage("received /bind with invalid label ${msg.toString()}");
      return;
    }
    controlService.bindControl(label);
  }

  // address: /unbind
  // args: [label]
  void unbindOsc(OSCMessage msg) {
    if (msg.arguments.length != 1) {
      messageService.addMessage(
          "received /unbind with malformed packet ${msg.toString()}");
      return;
    }
    var label = msg.arguments[0] as String;
    controlService.resetControl(label);
  }

  // address: /set
  // args: [label, float value]
  void setClientValue(OSCMessage msg) {
    if (msg.arguments.length != 2) {
      messageService
          .addMessage("received /set with malformed packet ${msg.toString()}");
    }

    var label = msg.arguments[0] as String;
    var val = msg.arguments[1] as double;
    controlService.updateControlClientValue(label, val);
  }

  void updateControlValue(int idx, double val) {
    controlService.updateControlValueByIndex(idx, val);
    osc?.send(OSCMessage(controlService.controls[idx].label, arguments: [val]));
  }

  void disconnectOsc() {
    osc?.close();
    _connected = false;

    controlService.resetAll();

    notifyListeners();
  }

  bool get connected => _connected;
}

class ControlService extends ChangeNotifier {
  final controls = [ControlInfo(), ControlInfo(), ControlInfo()];
  int _lastUpdateIndex = -1;

  MessageService messageService;

  ControlService({required this.messageService}) : super();

  resetControl(String label) {
    var existIdx = controls.indexWhere((c) => c.label == label);
    if (existIdx == -1) {
      // Not bound nothing to do
      messageService.addMessage("received /unbind $label but not bound");
      return;
    }

    controls[existIdx] = ControlInfo();
    _lastUpdateIndex = existIdx;
    notifyListeners();
  }

  void resetAll() {
    for (var i = 0; i < controls.length; i++) {
      controls[i] = ControlInfo();
    }
    _lastUpdateIndex = -1;
    notifyListeners();
  }

  bindControl(String label) {
    // What is the binding label, do we already have a binding?
    var existIdx = controls.indexWhere((c) => c.label == label);
    if (existIdx != -1) {
      // Already bound, just leave it
      // TODO when we support input types we could unbind and rebind to new input
      messageService.addMessage("received /bind $label but already bound");
      return;
    }

    // Check if there are any elements still available it
    // requests to bind to.
    var freeIdx = controls.indexWhere((c) => !c.isActive());
    if (freeIdx == -1) {
      // Nothing to do, we're full
      messageService.addMessage("received /bind $label but all slots full");
      return;
    }

    // if this checks out, update clientInfo and set active;
    controls[freeIdx].label = label;
    _lastUpdateIndex = freeIdx;
    notifyListeners();
  }

  updateControlClientValue(String label, double val) {
    var existIdx = controls.indexWhere((c) => c.label == label);
    if (existIdx == -1) {
      messageService.addMessage("received /set $label but $label not bound");
      return;
    }

    controls[existIdx].clientValue = val;
    _lastUpdateIndex = existIdx;
    notifyListeners();
  }

  updateControlValueByIndex(int idx, double val) {
    controls[idx].controlValue = val;
    _lastUpdateIndex = idx;
    notifyListeners();
  }

  bool didUpdateLast(int idx) {
    if (_lastUpdateIndex == -1) {
      return true;
    }
    return idx == _lastUpdateIndex;
  }
}

class ControlInfo {
  double clientValue;
  double controlValue;
  String label;

  ControlInfo(
      {this.clientValue = 0.0,
      this.controlValue = 0.0,
      this.label = NOT_SET_LABEL});

  bool isActive() {
    return this.label != NOT_SET_LABEL;
  }
}
